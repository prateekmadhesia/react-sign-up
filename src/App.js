import React, { Component } from 'react'
import './App.css'
import Form from './components/Form';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      users: []
    }
    this.newUser = this.newUser.bind(this);
  }
  

  newUser(obj){
    this.setState({users: [...this.state.users, obj]})
  }

  render() {
    // console.log(this.state);
    return (
      <div className="App">
        <Form dataToStore={this.newUser} />
      </div>
    )
  }
}
export default App;