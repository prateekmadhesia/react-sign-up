import React, { Component } from "react";
import "./form.css";

class Form extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    password: "",
    submitted: false,
    wrong: false,
    firstNameMessage: "",
    lastNameMessage: "",
    emailMessage: "",
    phoneMessage: "",
    passwordMessage: "",
  };
  updateFirstName = (change) => {
    this.setState({ firstName: change.target.value });
    if (change.target.value === "") {
      this.setState({ firstNameMessage: "Please, enter your first name" });

      if (typeof change.target.className.includes("is-valid")) {
        change.target.classList.remove("is-valid");
      }
      change.target.classList.add("is-invalid");
    } else {
      this.setState({ firstNameMessage: "" });

      if (typeof change.target.className.includes("is-invalid")) {
        change.target.classList.remove("is-invalid");
      }
      change.target.classList.add("is-valid");
    }
  };

  updateLastName = (change) => {
    this.setState({ lastName: change.target.value });
    if (change.target.value === "") {
      this.setState({ lastNameMessage: "Please, enter your last name" });

      if (typeof change.target.className.includes("is-valid")) {
        change.target.classList.remove("is-valid");
      }
      change.target.classList.add("is-invalid");
    } else {
      this.setState({ lastNameMessage: "" });

      if (typeof change.target.className.includes("is-invalid")) {
        change.target.classList.remove("is-invalid");
      }
      change.target.classList.add("is-valid");
    }
  };

  updatePhone = (change) => {
    this.setState({ phone: change.target.value });
    let valid = [...change.target.value]
      .map((number) => {
        return isNaN(number);
      })
      .filter((bool) => {
        return bool === true;
      })
      .includes(true);

    if (change.target.value === "") {
      this.setState({ phoneMessage: "Please, enter your phone number" });

      if (typeof change.target.className.includes("is-valid")) {
        change.target.classList.remove("is-valid");
      }
      change.target.classList.add("is-invalid");
    } else if (valid) {
      this.setState({ phoneMessage: "Please, enter valid phone number" });

      if (typeof change.target.className.includes("is-valid")) {
        change.target.classList.remove("is-valid");
      }
      change.target.classList.add("is-invalid");
    } else if (change.target.value.length !== 10) {
      this.setState({ phoneMessage: "Phone number should be of 10 digits" });

      if (typeof change.target.className.includes("is-valid")) {
        change.target.classList.remove("is-valid");
      }
      change.target.classList.add("is-invalid");
    } else {
      this.setState({ phoneMessage: "" });

      if (typeof change.target.className.includes("is-invalid")) {
        change.target.classList.remove("is-invalid");
      }
      change.target.classList.add("is-valid");
    }
  };

  updateEmail = (change) => {
    this.setState({ email: change.target.value });

    if (change.target.value === "") {
      this.setState({ emailMessage: "Please, enter your email" });

      if (typeof change.target.className.includes("is-valid")) {
        change.target.classList.remove("is-valid");
      }
      change.target.classList.add("is-invalid");
    } else if (
      !change.target.value.includes("@") ||
      !change.target.value.includes(".")
    ) {
      this.setState({ emailMessage: "Please, enter valid email" });

      if (typeof change.target.className.includes("is-valid")) {
        change.target.classList.remove("is-valid");
      }
      change.target.classList.add("is-invalid");
    } else {
      this.setState({ emailMessage: "" });

      if (typeof change.target.className.includes("is-invalid")) {
        change.target.classList.remove("is-invalid");
      }
      change.target.classList.add("is-valid");
    }
  };

  updatePassword = (change) => {
    this.setState({ password: change.target.value });

    if (change.target.value === "") {
      this.setState({ passwordMessage: "Please, enter your password" });

      if (typeof change.target.className.includes("is-valid")) {
        change.target.classList.remove("is-valid");
      }
      change.target.classList.add("is-invalid");
    } else if (change.target.value.length < 8) {
      this.setState({
        passwordMessage: "Password should be at least 8 characters",
      });

      if (typeof change.target.className.includes("is-valid")) {
        change.target.classList.remove("is-valid");
      }
      change.target.classList.add("is-invalid");
    } else {
      this.setState({ passwordMessage: "" });

      if (typeof change.target.className.includes("is-invalid")) {
        change.target.classList.remove("is-invalid");
      }
      change.target.classList.add("is-valid");
    }
  };

  isValid = () => {
    let result = false;

    if (this.state.firstName === "") {
      this.setState({ firstNameMessage: "Please, enter your first name" });
      result = true;
    } else {
      this.setState({ firstNameMessage: "" });
    }

    if (this.state.lastName === "") {
      this.setState({ lastNameMessage: "Please, enter your last name" });
      result = true;
    } else {
      this.setState({ lastNameMessage: "" });
    }

    if (this.state.phone === "") {
      this.setState({ phoneMessage: "Please, enter your phone number" });
      result = true;
    } else if (
      [...this.state.phone]
        .map((number) => {
          return isNaN(number);
        })
        .filter((bool) => {
          return bool === true;
        })
        .includes(true)
    ) {
      this.setState({ phoneMessage: "Please, enter valid phone number" });
      result = true;
    } else if (this.state.phone.length !== 10) {
      this.setState({ phoneMessage: "Phone number should be of 10 digits" });
      result = true;
    } else {
      this.setState({ phoneMessage: "" });
    }

    if (this.state.email === "") {
      this.setState({ emailMessage: "Please, enter your email" });
      result = true;
    } else if (
      !this.state.email.includes("@") ||
      !this.state.email.includes(".")
    ) {
      this.setState({ emailMessage: "Please, enter a valid email" });
      result = true;
    } else {
      this.setState({ emailMessage: "" });
    }

    if (this.state.password === "") {
      this.setState({ passwordMessage: "Please, enter your password" });
      result = true;
    } else if (this.state.password.length < 8) {
      this.setState({
        passwordMessage: "Password should be at least 8 digits",
      });
      result = true;
    } else {
      this.setState({ passwordMessage: "" });
    }

    this.setState({ wrong: false });
    return result;
  };

  onSubmit = (e) => {
    e.preventDefault();

    if (this.isValid()) {
      for (let formChild of e.target.children) {
        if (formChild.children[1] !== undefined) {
          let idName = formChild.children[1].id;
          let classes = formChild.children[1].className;

          if (idName === "fname") {
            if (
              this.state.firstName === "" ||
              this.state.firstNameMessage !== ""
            ) {
              if (!classes.includes("is-invalid")) {
                formChild.children[1].classList.add("is-invalid");
              }
            }
          } else if (idName === "lname") {
            if (
              this.state.lastName === "" ||
              this.state.lastNameMessage !== ""
            ) {
              if (!classes.includes("is-invalid")) {
                formChild.children[1].classList.add("is-invalid");
              }
            }
          } else if (idName === "phone") {
            if (this.state.phone === "" || this.state.phoneMessage !== "") {
              if (!classes.includes("is-invalid")) {
                formChild.children[1].classList.add("is-invalid");
              }
            }
          } else if (idName === "email") {
            if (this.state.email === "" || this.state.emailMessage !== "") {
              if (!classes.includes("is-invalid")) {
                formChild.children[1].classList.add("is-invalid");
              }
            }
          } else if (idName === "password") {
            if (
              this.state.password === "" ||
              this.state.passwordMessage !== ""
            ) {
              if (!classes.includes("is-invalid")) {
                formChild.children[1].classList.add("is-invalid");
              }
            }
          }
        }
      }
      return;
    }

    this.props.dataToStore(this.state);
    this.setState({
      submitted: true,
      wrong: false,
    });
  };

  render() {
    return (
      <div className=" contain">
        <form
          onSubmit={this.onSubmit}
          className="col-md-10 col-lg-8 col-sm-10 col-10
          needs-validation"
        >
          <div className="form-group">
            <label>First Name</label>
            <input
              type="text"
              id="fname"
              className="form-control"
              placeholder="Enter first name"
              value={this.state.firstName}
              onChange={this.updateFirstName}
            />
            <div className="valid-feedback">Looks good!</div>
            <div className="invalid-feedback">
              {this.state.firstNameMessage}
            </div>
          </div>
          <div className="form-group">
            <label>Last Name</label>
            <input
              type="text"
              id="lname"
              className="form-control"
              placeholder="Enter last name"
              value={this.state.lastName}
              onChange={this.updateLastName}
            />
            <div className="valid-feedback">Looks good!</div>
            <div className="invalid-feedback">{this.state.lastNameMessage}</div>
          </div>
          <div className="form-group">
            <label>Phone No.</label>
            <input
              type="text"
              className="form-control"
              id="phone"
              placeholder="Enter phone number"
              value={this.state.phone}
              onChange={this.updatePhone}
            />
            <div className="valid-feedback">Looks good!</div>
            <div className="invalid-feedback">{this.state.phoneMessage}</div>
          </div>
          <div className="form-group">
            <label>Email address</label>
            <input
              type="text"
              className="form-control"
              id="email"
              placeholder="Enter email"
              value={this.state.email}
              onChange={this.updateEmail}
            />
            <div className="valid-feedback">Looks good!</div>
            <div className="invalid-feedback">{this.state.emailMessage}</div>
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control"
              id="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.updatePassword}
            />
            <div className="valid-feedback">Looks good!</div>
            <div className="invalid-feedback">{this.state.passwordMessage}</div>
          </div>

          <div className="button-contain">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </form>
        <div
          className="successMessage"
          style={
            this.state.submitted ? { display: "block" } : { display: "none" }
          }
        >
          <h3 className="alert alert-success">Successfully Sign up!!</h3>{" "}
        </div>
      </div>
    );
  }
}
export default Form;
